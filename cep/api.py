# -*- coding: utf-8 -*-

import requests

# TODO - Refactoring - Codigo muito repetitivo, fazer metodos que deixem isso mais limpo
# TODO - PP - Os atributos TOKEN e headers são publicos, use Private Class para o caso
#

class CEPAPI:

    def __init__(self, token):
        self.TOKEN = token
        self.headers = {'Authorization': 'Token token=%s' % self.TOKEN}

    def getPorCEP(self, cep):
        url = "http://www.cepaberto.com/api/v3/cep"
        params = { 'cep': cep }
        json = requests.get(url, params=params, headers=self.headers).json()
        return json

    def getPorCidade(self, uf, cidade, logradouro):
        # Pesquisa de CEP por estado, cidade e logradouro
        url = "http://www.cepaberto.com/api/v3/address"
        params = {'estado': uf, 'cidade': cidade, 'logradouro': logradouro}
        json = requests.get(url, params=params, headers=self.headers).json()
        return json

    # Pesquisa pelos nomes das cidades em SP
    def listaCidadesPorUf(self, uf):
        url = "http://www.cepaberto.com/api/v3/cities"
        params = { 'estado' : uf }
        json = requests.get(url, params=params, headers=self.headers).json()
        return json

    # Busca em um Raio de 10km
    def getCEPMaisProximo(self, lat, lng):
        url = "http://www.cepaberto.com/api/v3/nearest"
        params = { 'lat' : lat, 'lng': lng }
        json  = requests.get(url, params=params, headers=self.headers).json()
        return json
